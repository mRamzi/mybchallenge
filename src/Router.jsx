import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';

import ProfilePage from './Pages/ProfilePage';
import UpcomingEventsTab from './Pages/TabBar'
import FriendsTab from './Pages/FriendsTab'
import TabBar from './Pages/TabBar'
import FriendCard from './Pages/FriendCard'
import App from './App';


export default function() {
   return (
      <Router>
            <Route path="/" Component={App} />
      </Router>
   );
}
