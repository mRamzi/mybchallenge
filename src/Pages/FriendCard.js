import React, { Component } from "react";

import Row from "react-bootstrap/Row";
import Image from "react-bootstrap/Image";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";

class Friendcard extends Component {
    render() {
      return (
        <Col md="4" style={{ paddingBottom: 20 }}>
          <Card
            style={{
              width: "360px",
              paddingBottom: 20,
              paddingTop: 10
            }}
          >
            <Row>
              <Col md="3">
                <Image
                  fluid
                  roundedCircle
                  style={{
                    objectFit: "scale-down",
                    paddingTop: 10,
                    marginLeft: 10
                  }}
                  src={this.props.friend.picture}
                  alt="Card image cap"
                />
              </Col>
              <Col md="7">
                <Row>
                   <Card.Link href={'../'+this.props.friend.id+'/upcoming'} style={{ fontSize: 20, float: "left", paddingTop: 15, paddingLeft: 10 }}>
{this.props.friend.first_name}  {this.props.friend.last_name}          </Card.Link>
                </Row>
                <Row>
                  <Card.Text style={{ fontSize: 10,paddingLeft: 10 }}>{this.props.friend.total_events} events</Card.Text>
                </Row>
                <Row>
                  <Card.Text style={{ fontSize: 10,paddingLeft: 10 }}>{this.props.friend.total_friends} friends</Card.Text>
                </Row>
              </Col>
            </Row>
          </Card>
        </Col>

      )
    }


  }

export default Friendcard;