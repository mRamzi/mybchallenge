import React, { Component } from "react";
import styles from "./UserPanel.module.css";
import Image from "react-bootstrap/Image";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Fetch from "../Components/Fetch";
import Moment from 'react-moment';
import frenchStrings from "react-timeago/lib/language-strings/fr";
import buildFormatter from "react-timeago/lib/formatters/buildFormatter";

class UserPanel extends Component {
  render() {
    return (
      <Container style={{ width: 250 }}>
        <Fetch
          queryPlayer={this.props.queryPlayer+this.props.match.params.id}
          action={this.props.action}
        />
        <Card
          style={{
            width: "match_parent",
            height: "wrap_content",
            borderRadius: "90px 10px 50px 35px"
          }}
        >
          <Col>
            <Image
              src={this.props.user.picture}
              fluid
              roundedCircle
              style={{
                objectFit: "cover",
                width: 300,
                height: 220,
                paddingTop: 20,
                paddingBottom: 10
              }}
            />
          </Col>

          <Col>
            <Card.Body style={{ border: "solid 0px", padding: 3 }}>
              <Col md="12">
                <Row>
                  <Card.Title>{this.props.user.first_name}  {this.props.user.last_name}</Card.Title>
                </Row>
                <Row>
                  <Card.Subtitle
                    className="mb-2 text-muted"
                    style={{ paddingRight: 20, textAlign: "left" }}
                  >
                    Works at {this.props.user.company}
                  </Card.Subtitle>
                </Row>
              </Col>
              <Card.Text
                style={{ paddingRight: 20, textAlign: "left", fontSize: 10 }}
              >
                Lives in {this.props.user.city_name}
              </Card.Text>
              <Card.Text
                style={{
                  paddingRight: 20,
                  textAlign: "left",
                  fontSize: 10,
                  margin: 0
                }}
              >
                Last seen <Moment fromNow >{this.props.user.last_seen}</Moment>
              </Card.Text>
              <hr style={{ borderTop: "3px double #8c8b8b" }} />
              <Container
                style={{ padding: 0, paddingTop: 10, paddingBottom: 30 }}
              >
                <Row>
                  <Col md="6">
                    <Card>
                      <Card.Body>
                        <Card.Title> {this.props.user.total_events} </Card.Title>
                        <Card.Text style={{ fontSize: 10 }}> Events </Card.Text>
                      </Card.Body>
                    </Card>
                  </Col>
                  <Col md="6">
                    <Card>
                      <Card.Body>
                        <Card.Title> {this.props.user.total_friends} </Card.Title>
                        <Card.Text style={{ fontSize: 10 }}>
                          {" "}
                          Friends{" "}
                        </Card.Text>
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>
              </Container>
            </Card.Body>
          </Col>
        </Card>
      </Container>
    );
  }
}

export default UserPanel;
