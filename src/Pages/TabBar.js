import React, { Component } from "react";
import Container from "react-bootstrap/Container";
import FriendsTab from "./FriendsTab";
import EventsTab from "./UpcomingEventsTab";
import Row from "react-bootstrap/Row";
import { Nav } from "react-bootstrap";
import ProfilePage from "./ProfilePage";
import { RoutedTabs, NavTab } from "react-router-tabs";
import { BrowserRouter, Route, Link, Switch, Redirect } from "react-router-dom";
class TabBar extends Component {
  render() {
    return (
      <Container>
        <BrowserRouter>
          <Nav fill variant="tabs" defaultActiveKey="/friends">
            <Nav.Item>
              <Nav.Link>
                <Link
                  to={{
                    pathname: `/players/${this.props.match.params.id}/upcoming`
                  }}
                >
                  Upcoming events
                </Link>
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link>
                <Link
                  to={{
                    pathname: `/players/${this.props.match.params.id}/friends`
                  }}
                >
                  Friends
                </Link>
              </Nav.Link>
            </Nav.Item>
          </Nav>
          <Switch>
            <Route
              path="/players/:id/upcoming"
              render={props => (
                <EventsTab
                  {...props}
                  actionEvents={this.props.actionEvents}
                  queryEvents={this.props.queryEvents}
                  eventList={this.props.eventList}
                />
              )}
            />
            <Route
              path="/players/:id/friends"
              render={props => (
                <FriendsTab
                  {...props}
                  queryFriends={this.props.queryFriends}
                  action={this.props.action}
                  list={this.props.list}
                />
              )}
            />
            <Redirect to="/players/:id/upcoming" />
          </Switch>
        </BrowserRouter>
      </Container>
    );
  }
}

export default TabBar;
