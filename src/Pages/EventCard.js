import React, { Component } from "react";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Moment from "react-moment";
function EventCard(props) {
    return (
      <Col md="4">
        <Card style={{ width: "20rem", height: "12rem", marginBottom: 20 }}>
          <Card.Body>
            <Row>
              <Col md="6">
                <Card.Text style={{ fontSize: 15, float: "left" }}>
                  in{" "}
                  <Moment fromNow ago>
                    {props.event.date}
                  </Moment>
                </Card.Text>
              </Col>
            </Row>
            <Row>
              <Col md="2">
                <Card.Text
                  style={{
                    paddingTop: 0,
                    float: "left",
                    fontSize: 30,
                    fontWeight: "bold"
                  }}
                >
                  {props.event.name}
                </Card.Text>
              </Col>
            </Row>
            <Row>
              <Col md="7">
                <Card.Text
                  style={{
                    paddingTop: 0,
                    float: "left",
                    fontSize: 16,
                    fontWeight: "light"
                  }}
                >
                  <p align="left">
                    <Moment format="dddd Do MMMM [at] HH:mm">
                      {props.event.date}
                    </Moment>
                  </p>
                </Card.Text>
              </Col>
            </Row>
            <Row>
              <Col md="7">
                <Card.Text
                  style={{
                    paddingTop: 0,
                    float: "left",
                    fontSize: 16,
                    fontWeight: "light"
                  }}
                >
                  <p align="left">
                    {props.event.participants.length} participants
                  </p>
                </Card.Text>
              </Col>
            </Row>
          </Card.Body>
        </Card>
      </Col>
    );
  
}

export default EventCard;
