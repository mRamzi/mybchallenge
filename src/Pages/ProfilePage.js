import React, { Component } from "react";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import UserPanel from "./UserPanel";
import TabBar from "./TabBar";
import Fetch from "../Components/Fetch";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.getChildUser = this.getChildUser.bind(this);
    this.getFriendsList = this.getFriendsList.bind(this);
    this.getEventsList = this.getEventsList.bind(this);

    this.state = {
      playername: null,
      user: {},
      listofFriends: {},

      listofEvents: {},
    };
  }

  getChildUser(user) {
    this.setState({
      user: user
    });
  }

  getFriendsList(friendsList) {
    this.setState({
      listofFriends: friendsList
    });
  }

  getEventsList(eventsList) {
    this.setState({
      listofEvents: eventsList
    });
  }

  render() {
    return (
      <Container
        style={{
          paddingTop: 30,
          maxWidth: 1500,
          paddingBottom: 30
        }}
        fluid
      >
        {" "}
        <BrowserRouter>
        <Switch>
          <Row>
            <Route
              path="/players/:id/"
              render={props => (
                <UserPanel
                  {...props}
                  user={this.state.user}
                  queryPlayer="/players/"
                  action={this.getChildUser}
                />
              )}
            />

            <Route
              path="/players/:id/"
              render={props => (
                <TabBar
                  {...props}
                  list={this.state.listofFriends}
                  eventList={this.state.listofEvents}
                  queryFriends="/players/1/friends"
                  queryEvents="/players/1/lastEvents"
                  action={this.getFriendsList}
                  actionEvents={this.getEventsList}
                />
              )}
            />

          </Row>
          </Switch>
        </BrowserRouter>
      </Container>
    );
  }
}

export default ProfilePage;
