import React, { Component } from "react";
import styles from "./UserPanel.module.css";
import Image from "react-bootstrap/Image";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Fetch from "../Components/Fetch";
import EventCard from "./EventCard";

class UpcomingEventsTab extends Component {
  render() {
    const data = Object.values(this.props.eventList);

    return (
      <Container style={{marginTop: 40}}>
        <Fetch actionEvents={this.props.actionEvents} queryEvents={this.props.queryEvents} queryId={this.props.match.params.id}/>
        <Row>
          <h5 style={{ float: "left" }}>Upcoming events</h5>
        </Row>
        <Row>
        {data.map((item, index) => {
          return (
           
               <Col md="4">
                  <EventCard event={item} />
                  </Col>
            
          );
        })}
        </Row>
      </Container>
    );
  }
}

export default UpcomingEventsTab;
