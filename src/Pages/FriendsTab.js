import React, { Component } from "react";
import Row from "react-bootstrap/Row";
import Fetch from "../Components/Fetch";
import FriendCard from "./FriendCard";
import Infinite from 'react-infinite';
class FriendsTab extends Component {
  render(){
  const data = Object.values(this.props.list);

  return (
    <div style={{ marginTop: 40 }}>
      <Fetch queryFriends={this.props.queryFriends} action={this.props.action} queryId={this.props.match.params.id}/>
      <Row>
        <h5 style={{ float: "left" }}>Friends</h5>
      </Row>
     
      <Infinite containerHeight={800} elementHeight={1000}>
      <Row>
        {data.map((item, index) => {
          return <FriendCard friend={item} />;
        })}
        
      </Row>
</Infinite>
      
    </div>
  );
      }
}

export default FriendsTab;
