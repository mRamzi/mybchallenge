import React, { Component } from 'react';
import './App.css';

import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import ProfilePage from './Pages/ProfilePage';


class App extends Component {
  render() {
    return (
       <div className="App" style={{paddingTop: 30}}>
          <Router >
          
          <Route path="/players" component={ProfilePage} /> 
             </Router>
       </div>
    );
 }
}

export default App;
