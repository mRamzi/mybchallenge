import React, { Component } from "react";
import axios from "axios";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Image from "react-bootstrap/Image";

const API = "http://localhost:5000";

class Fetch extends Component {
  componentDidMount() {
    if (this.props.queryPlayer ) {
      axios
        .get(API  + this.props.queryPlayer)
        .then(response => this.props.action(response.data));
    } else if (this.props.queryFriends) {
      axios.get(API + '/players/' + this.props.queryId +'/friends').then(response => {
        this.props.action(response.data);
      });
    } else if (this.props.queryEvents) {
      axios.get(API + '/players/'+ this.props.queryId+'/lastEvents').then(response => {
        this.props.actionEvents(response.data);
      });
    }
  }

  render() {
    return "";
  }
}

export default Fetch;
